## Solution Overview

 - Spring Boot HTTP server running on Tomcat
 - Spring Data used for interaction with Mongo
 - Cucumber acceptance features running on top of Spring Boot integration tests
 - Unit tests for core logic only
 - Logback used for logs management

## Instructions

 - `mvn clean test` will run all tests
 - `mvn package` will create a fat JAR called *offer-service.jar* under *target* folder
 - `java -jar offer-service.jar` will start a Tomcat server on port 4001
 - There are also some IntelliJ run configurations if you import this in IntelliJ.
 
## API Documentation
 
| HTTP Method | Endpoint                         |
| ----------- | -------------------------------- |
| POST        | http://localhost:4001/offers     |
| GET         | http://localhost:4001/offers/{id}|

 - The POST endpoint takes a request body in the following form
 
 ```
 {
    "title": "Mazda3",
    "description": "Model 2016",
    "price: 12500,
    "currencyCode": "GBP"
 }
 ```
 
## Dependencies
 
 - The application interacts with a Mongo database (which has been configured as *localhost:27017*)
 - Acceptance tests will fail if there is no Mongo database running at the specified address
 
## Notes
 
 - in reality, the application would accept an external properties file as well (to override the properties defined in the classpath, usually specific to each environment)
 - also, it would accept an external logback file (at the very least, production would be configured specifically)
 

=======================================================================================================================


##Background

Per Wikipedia, "an offer is a proposal to sell a specific product or service under specific conditions". As a merchant I offer goods for sale. I want to create an offer so that I can share it with my customers.
All my offers have shopper friendly descriptions. I price all my offers up front in a defined currency.

##Assignment

You are required to create a simple RESTful software service that will allow a merchant to create a new simple offer.

###Guidelines

 - The solution should be written in Java 
 - No restrictions on external libraries
 - Please do no include dependent binaries; use a dependency management tool (Maven, Gradle, Ivy)
 - Submit as a git repository (link to GitHub, BitBucket, etc)
 - The merchant should be able to interact with the service over HTTP
 - We are looking for a simple solution representative of an enterprise deliverable
 - Use TDD
 - Please pay attention to OO design; clean code, adherence to SOLID principles
 - As a simplification offers my be persisted to a file, embedded database or merely in memory
 - You can ignore authentication and authorization concerns
 - Feel free to make any assumptions and include in a README.md file, or otherwise, with the submission