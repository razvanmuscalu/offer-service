package com.worldpay.test.offer_service.exception;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import static org.springframework.http.ResponseEntity.badRequest;

@ControllerAdvice(basePackages = "com.worldpay.test.offer_service")
public class ExceptionControllerAdvice {

    @ExceptionHandler(InvalidOfferException.class)
    public ResponseEntity handleInvalidOfferException(InvalidOfferException ex) {
        ErrorResponse response = new ErrorResponse(ex.getMessage());
        return badRequest().body(response);
    }
}
