package com.worldpay.test.offer_service.exception;

public class InvalidOfferException extends RuntimeException {

    public InvalidOfferException(String message) {
        super(message);
    }
}
