package com.worldpay.test.offer_service.exception;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ErrorResponse {

    private String message;

    public ErrorResponse(@JsonProperty("message") String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

}
