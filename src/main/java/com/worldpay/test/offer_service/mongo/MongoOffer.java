package com.worldpay.test.offer_service.mongo;

import com.worldpay.test.offer_service.Offer;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import static org.apache.commons.lang3.builder.ToStringBuilder.reflectionToString;
import static org.apache.commons.lang3.builder.ToStringStyle.SHORT_PREFIX_STYLE;

@Document(collection = "offer")
public class MongoOffer extends Offer {

    @Id
    public String id;

    public MongoOffer(String title,
                      String description,
                      Integer price,
                      String currencyCode) {
        super(title, description, price, currencyCode);
    }

    public String getId() {
        return id;
    }

    @Override
    public String toString() {
        return reflectionToString(this, SHORT_PREFIX_STYLE);
    }
}
