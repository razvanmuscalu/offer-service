package com.worldpay.test.offer_service.mongo;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface OfferRepository extends MongoRepository<MongoOffer, String> {
}
