package com.worldpay.test.offer_service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

import static org.springframework.http.ResponseEntity.created;
import static org.springframework.http.ResponseEntity.ok;

@RestController
public class OffersController {

    private static final String GET_OFFERS = "/offers/{id}";

    private final OffersService offersService;

    @Autowired
    public OffersController(OffersService offersService) {
        this.offersService = offersService;
    }

    @PostMapping("/offers")
    public ResponseEntity<Void> postOffers(@RequestBody Offer offer,
                                           UriComponentsBuilder uriComponentsBuilder) {

        String insertedOfferId = offersService.addOffer(offer);

        URI location = uriComponentsBuilder.path(GET_OFFERS).buildAndExpand(insertedOfferId).toUri();
        return created(location).build();
    }

    @GetMapping("/offers/{id}")
    public ResponseEntity<Offer> getOfferById(@PathVariable("id") String id) {
        return ok(offersService.getOffer(id));
    }
}
