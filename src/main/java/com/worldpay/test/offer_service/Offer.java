package com.worldpay.test.offer_service;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import static org.apache.commons.lang3.builder.ToStringBuilder.reflectionToString;
import static org.apache.commons.lang3.builder.ToStringStyle.SHORT_PREFIX_STYLE;

@JsonIgnoreProperties("id")
public class Offer {

    private final String title;
    private final String description;
    private final Integer price;
    private final String currencyCode;

    public Offer(@JsonProperty("title") String title,
                 @JsonProperty("description") String description,
                 @JsonProperty("price") Integer price,
                 @JsonProperty("currencyCode") String currencyCode) {
        this.currencyCode = currencyCode;
        this.title = title;
        this.description = description;
        this.price = price;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public Integer getPrice() {
        return price;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    @Override
    public String toString() {
        return reflectionToString(this, SHORT_PREFIX_STYLE);
    }

}
