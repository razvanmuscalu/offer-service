package com.worldpay.test.offer_service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.worldpay.test.offer_service")
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}