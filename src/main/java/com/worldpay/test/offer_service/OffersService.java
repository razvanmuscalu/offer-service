package com.worldpay.test.offer_service;

import com.worldpay.test.offer_service.exception.InvalidOfferException;
import com.worldpay.test.offer_service.mongo.MongoOffer;
import com.worldpay.test.offer_service.mongo.OfferRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static java.util.Currency.getInstance;
import static org.apache.commons.lang3.StringUtils.isEmpty;

@Component
public class OffersService {

    private static final String TITLE_MISSING_MESSAGE = "Title not set";
    private static final String PRICE_MISSING_MESSAGE = "Price not set";
    private static final String CURRENCY_MISSING_MESSAGE = "Currency not set";
    private static final String CURRENCY_INVALID_MESSAGE = "Currency not valid";
    private static final int ZERO = 0;

    private final OfferRepository offerRepository;

    @Autowired
    public OffersService(OfferRepository offerRepository) {
        this.offerRepository = offerRepository;
    }

    public String addOffer(Offer offer) {
        validateOffer(offer);

        MongoOffer insertedOffer = offerRepository.insert(new MongoOffer(offer.getTitle(), offer.getDescription(), offer.getPrice(), offer.getCurrencyCode()));

        return insertedOffer.getId();
    }

    public Offer getOffer(String id) {
        return offerRepository.findOne(id);
    }

    private void validateOffer(Offer offer) {
        if (isEmpty(offer.getTitle()))
            throw new InvalidOfferException(TITLE_MISSING_MESSAGE);

        if (offer.getPrice() == null || offer.getPrice() == ZERO)
            throw new InvalidOfferException(PRICE_MISSING_MESSAGE);

        if (isEmpty(offer.getCurrencyCode()))
            throw new InvalidOfferException(CURRENCY_MISSING_MESSAGE);

        try {
            getInstance(offer.getCurrencyCode());
        } catch (IllegalArgumentException ex) {
            throw new InvalidOfferException(CURRENCY_INVALID_MESSAGE);
        }
    }
}
