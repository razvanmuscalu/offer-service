package com.worldpay.test.offer_service;

import com.worldpay.test.offer_service.exception.InvalidOfferException;
import com.worldpay.test.offer_service.mongo.MongoOffer;
import com.worldpay.test.offer_service.mongo.OfferRepository;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

@RunWith(Enclosed.class)
public class OffersServiceTest {

    @RunWith(Parameterized.class)
    public static class ParameterizedTest {

        @Parameterized.Parameters(name = "addOffer[{0},{1}]:{2}")
        public static Iterable<Object[]> data() {
            return Arrays.asList(new Object[][]{
                    {null, 12500, "GBP", "Title not set"},
                    {"", 12500, "GBP", "Title not set"},
                    {"Mazda3", null, "GBP", "Price not set"},
                    {"Mazda3", 0, "GBP", "Price not set"},
                    {"Mazda3", 12500, null, "Currency not set"},
                    {"Mazda3", 12500, "", "Currency not set"},
                    {"Mazda3", 12500, "AAA", "Currency not valid"},
            });
        }

        @Mock
        private OfferRepository offerRepository;

        @Mock
        private MongoOffer mongoOffer;

        @Rule
        public ExpectedException thrown = ExpectedException.none();

        private OffersService sut;

        private final String title;
        private final Integer price;
        private final String currencyCode;
        private final String expectedResult;

        public ParameterizedTest(String title, Integer price, String currencyCode, String expectedResult) {
            this.title = title;
            this.price = price;
            this.currencyCode = currencyCode;
            this.expectedResult = expectedResult;
        }

        @Before
        public void setUp() {
            initMocks(this);
            sut = new OffersService(offerRepository);
        }

        @Test
        public void shouldValidateOffer() {
            thrown.expect(InvalidOfferException.class);
            thrown.expectMessage(expectedResult);

            when(offerRepository.insert(any(MongoOffer.class))).thenReturn(mongoOffer);
            when(mongoOffer.getId()).thenReturn("any");

            String result = sut.addOffer(new Offer(title, "any", price, currencyCode));
            assertThat(result).isEqualTo(expectedResult);
        }
    }

    @RunWith(MockitoJUnitRunner.class)
    public static class NonParameterizedTest {

        @Mock
        private OfferRepository offerRepository;

        @Mock
        private MongoOffer mongoOffer;

        @InjectMocks
        private OffersService sut;

        @Test
        public void shouldAddOffer() {
            when(offerRepository.insert(any(MongoOffer.class))).thenReturn(mongoOffer);
            when(mongoOffer.getId()).thenReturn("any");

            String result = sut.addOffer(new Offer("Mazda3", "Model 2016", 12500, "GBP"));
            assertThat(result).isEqualTo("any");
        }
    }
}
