package com.worldpay.test.offer_service.acceptance;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OfferLocal {

    @JsonProperty("title")
    private String title;

    @JsonProperty("description")
    private String description;

    @JsonProperty("price")
    private String price;

    @JsonProperty("currencyCode")
    private String currencyCode;

}
