package com.worldpay.test.offer_service.acceptance;

import com.worldpay.test.offer_service.Offer;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.net.URI;

@Component
public class ResponseHolder {

    private ResponseEntity<String> response;

    public void set(ResponseEntity<String> response) {
        this.response = response;
    }

    public Integer geStatusCodeValue() {
        return response.getStatusCodeValue();
    }

    public String getBody() {
        return response.getBody();
    }

    public HttpHeaders getHeaders() {
        return response.getHeaders();
    }

    public URI getLocation() {
        return response.getHeaders().getLocation();
    }

}
