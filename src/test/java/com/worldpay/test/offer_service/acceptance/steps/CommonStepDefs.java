package com.worldpay.test.offer_service.acceptance.steps;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.worldpay.test.offer_service.Offer;
import com.worldpay.test.offer_service.acceptance.OfferLocal;
import com.worldpay.test.offer_service.acceptance.ResponseHolder;
import com.worldpay.test.offer_service.acceptance.SpringTest;
import com.worldpay.test.offer_service.exception.ErrorResponse;
import com.worldpay.test.offer_service.mongo.MongoOffer;
import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

@SpringTest
public class CommonStepDefs {

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    @Value("${offers.path}")
    private String offersPath;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private ResponseHolder responseHolder;

    @Autowired
    private MongoRepository<MongoOffer, String> mongoRepository;

    @Before
    public void setUp() {
        mongoRepository.deleteAll();
    }

    @When("^I post the following offer$")
    public void postOffer(DataTable dataTable) {
        OfferLocal offer = dataTable.asList(OfferLocal.class).get(0);

        ResponseEntity<String> response = restTemplate.postForEntity(offersPath, offer, String.class);
        responseHolder.set(response);
    }

    @Then("^the HTTP response status should be (.*)$")
    public void checkResponseStatus(Integer expectedStatus) {
        Integer actualStatus = responseHolder.geStatusCodeValue();
        assertThat(actualStatus).isEqualTo(expectedStatus);
    }

    @Then("^the HTTP response headers should include$")
    public void theHttpResponseHeadersShouldInclude(DataTable dataTable) {
        Map<String, String> expectedHeaders = dataTable.asMap(String.class, String.class);
        Map<String, String> actualHeaders = responseHolder.getHeaders().toSingleValueMap();

        assertThat(expectedHeaders.entrySet()).allMatch(p -> actualHeaders.get(p.getKey()).contains(p.getValue()));
    }

    @When("^I follow the Location header$")
    public void iFollowTheLocationHeader() {
        ResponseEntity<String> response = restTemplate.getForEntity(responseHolder.getLocation(), String.class);
        responseHolder.set(response);
    }

    @Then("^the following offer is returned$")
    public void theFollowingOfferIsReturned(DataTable dataTable) throws IOException {
        Offer expectedOffer = dataTable.asList(Offer.class).get(0);
        Offer actualOffer = OBJECT_MAPPER.readValue(responseHolder.getBody(), Offer.class);

        assertThat(actualOffer)
                .extracting("title", "price", "description")
                .contains(
                        expectedOffer.getTitle(),
                        expectedOffer.getPrice(),
                        expectedOffer.getDescription()
                );
    }

    @Then("^the HTTP response message should be '(.*)'$")
    public void theHTTPResponseMessageShouldBeTitleNotSet(String message) throws IOException {
        ErrorResponse actualErrorResponse = OBJECT_MAPPER.readValue(responseHolder.getBody(), ErrorResponse.class);
        assertThat(actualErrorResponse.getMessage()).isEqualTo(message);
    }
}
