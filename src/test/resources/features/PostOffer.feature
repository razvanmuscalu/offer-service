Feature: Post Offer


  Scenario: Should return CREATED status and Location header
    When I post the following offer
      | title  | price | currencyCode |
      | Mazda3 | 12500 | GBP          |
    Then the HTTP response status should be 201
    And the HTTP response headers should include
      | Location | http://localhost:4001/offers/ |


  Scenario: Should return BAD REQUEST if 'title' is missing
    When I post the following offer
      | title | price | currencyCode |
      |       | 12500 | GBP          |
    Then the HTTP response status should be 400
    And the HTTP response message should be 'Title not set'


  Scenario: Should return BAD REQUEST if 'price' is missing
    When I post the following offer
      | title  | price | currencyCode |
      | Mazda3 |       | GBP          |
    Then the HTTP response status should be 400
    And the HTTP response message should be 'Price not set'


  Scenario: Should return BAD REQUEST if 'price' is zero
    When I post the following offer
      | title  | price | currencyCode |
      | Mazda3 | 0     | GBP          |
    Then the HTTP response status should be 400
    And the HTTP response message should be 'Price not set'


  Scenario: Should return BAD REQUEST if 'price' is not number
    When I post the following offer
      | title  | price | currencyCode |
      | Mazda3 | any   | GBP          |
    Then the HTTP response status should be 400


  Scenario: Should return CREATED even if 'description' is missing
    When I post the following offer
      | title  | price | currencyCode | description |
      | Mazda3 | 12500 | GBP          |             |
    Then the HTTP response status should be 201


  Scenario: Should return BAD REQUEST if 'currencyCode' is missing
    When I post the following offer
      | title  | price | currencyCode |
      | Mazda3 | 12500 |              |
    Then the HTTP response status should be 400
    And the HTTP response message should be 'Currency not set'


  Scenario: Should return BAD REQUEST if 'currencyCode' is not valid
    When I post the following offer
      | title  | price | currencyCode |
      | Mazda3 | 12500 | AAA          |
    Then the HTTP response status should be 400
    And the HTTP response message should be 'Currency not valid'


  Scenario: Should return created offer when following the Location header
    Given I post the following offer
      | title  | price | description | currencyCode |
      | Mazda3 | 12500 | Model 2016  | GBP          |
    When I follow the Location header
    Then the following offer is returned
      | title  | price | description | currencyCode |
      | Mazda3 | 12500 | Model 2016  | GBP          |